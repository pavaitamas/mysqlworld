# Docker (MySQL + World Database)

#### Contains two World databases:

- "world" : [original one](https://downloads.mysql.com/docs/world.sql.zip)
- "world2" : modified table-column names to lowercase and underscored (due to compatibility issues)

#### Run with the following command:

- ```docker run --name mysqlworld -p 3306:3306 -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -d pavaitamas/mysqlworld```

